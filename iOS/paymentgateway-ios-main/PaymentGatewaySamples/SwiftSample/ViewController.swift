//
//  ViewController.swift
//  SwiftSample
//
//  Created by Senthil Kumar Selvaraj on 03/05/21.
//


import UIKit
import PaymentGatewaySwift

class ViewController: UIViewController {
    
    @IBOutlet weak var responseTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ViewController{
    
    @IBAction func payBittonPressed(){
        
        var params = PaymentGatewayParams(
            apiKey: SampleAppConstant.API_KEY,
            orderId: SampleAppConstant.ORDER_ID,
            hash: SampleAppConstant.HASH,
            mode: SampleAppConstant.MODE,
            amount: SampleAppConstant.AMOUNT,
            name: SampleAppConstant.NAME,
            phone: SampleAppConstant.PHONE,
            email: SampleAppConstant.EMAIL,
            returnURL: SampleAppConstant.RETURN_URL,
            description: SampleAppConstant.DESCRIPTION,
            currency: SampleAppConstant.CURRENCY,
            country: SampleAppConstant.COUNTRY,
            city: SampleAppConstant.CITY,
            state: SampleAppConstant.STATE,
            addressLine1: SampleAppConstant.ADD_1,
            addressLine2: SampleAppConstant.ADD_2,
            zipCode: SampleAppConstant.ZIP_CODE)
        
        // This param should be mandatory only for UPI intent payment
        params.interface_type = "ios_sdk"
        
        /// This is the alternate way to define all those params

        //        var params1 = PaymentGatewayParams()
        //        params1.timeout_duration = "20"
        //        params1.api_key = "xxxx-xxxx-xx-xxxxx"
        //        params1.enable_auto_refund = "n"
        
        /// This is for merchant
        let url = "<htttp url>" // Place the merchant payment URL
        PaymentGateway.open(controller: self, url: url, params: params, delegate: self)
    }
}


extension ViewController: PaymentGatewayDelegate{
    
    ///
    /// This is the main deleagte method that provides the conteollet object and data (String)
    ///
    
    func didPaymentCompleted(_ controller: UIViewController, data: Any?) {
        if let jsonString = data as? String, let response = jsonString.toJSONDictionary{
            if let statusCode = response.value(forKey: "response_code") as? Int, statusCode == 0{
                // Successs
                print("Success")
                responseTextView.text = jsonString
                print(response.value(forKey: "transaction_id"))
            }else{
                // Failure
                responseTextView.text = jsonString
                print("Failure")
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    ///
    /// This method indicates to cancel the payment screen
    ///
    
    func didPaymentCanceled(_ controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


/// SampleConstant things


struct SampleAppConstant {
    // API Key that is provided by Merchant Payament Portal.
    static let API_KEY = "<YOUR API KEY>"
    // Specify the clients web server url to catch the payemnt response
    static let RETURN_URL = "http://localhost:8888/paymentresponse"
    // Specify the Payamnet Mode. 1. TEST - Sandbox Mode 2. LIVE - Need to speicfy this one while go LIVE
    static let MODE = "LIVE"
    // Specify the Currency value. Only allowed INR
    static let CURRENCY = "INR"
    // Specify the Country code. Only allowed IND
    static let COUNTRY = "IND"
    /// Update your server generated HASH Key
    static let HASH = "<YOUR SERVER GENERATED HASH KEY>"
    static let ORDER_ID = "TEST2007"
    static let AMOUNT = "2"
    static let NAME = "Senthil"
    static let EMAIL = "emailsenthil@test.com"
    static let PHONE = "9597403366"
    static let DESCRIPTION = "Test"
    static let CITY = "Chennai"
    static let STATE = "Tamilnadu"
    static let ADD_1 = "ad1"
    static let ADD_2 = "ad2"
    static let ZIP_CODE = "630501"
    static let UDF_1 = ""
    static let UDF_2 = ""
    static let UDF_3 = ""
    static let UDF_4 = ""
    static let UDF_5 = ""
}

extension String{
    var toJSONDictionary: NSDictionary?{
        
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        
        return try? JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
    
    }
}
