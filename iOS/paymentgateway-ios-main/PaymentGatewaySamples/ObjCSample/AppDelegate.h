//
//  AppDelegate.h
//  ObjCSample
//
//  Created by Senthil Kumar Selvaraj on 03/05/21.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic, strong) UIWindow *window;

@end

