//
//  PaymentGatewayViewController.h
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/8/20.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "PaymentGatewayViewController.h"
#import <PaymentGatewayObjC/PaymentGatewayDelegate.h>
#import <PaymentGatewayObjC/PaymentGatewayParams.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentGatewayViewController : UIViewController<WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler>
/** @brief PaymentGateway Post request Codabale Model. That used for loading the payamnet request*/
@property(nonatomic, strong) PaymentGatewayParams *paymentGatewayParams;

@property(nonatomic, strong) NSString *url;

@property(nonatomic, strong) NSString *transactionId;

@property(nonatomic, strong) NSString *upiUri;

@property(nonatomic) BOOL isUPIPaymentSelected;

@property(nonatomic, nullable) NSTimer *timer;

/** @brief Webview that loads the payment request page*/
@property (strong, nonatomic)  WKWebView *paymentGatewayWebView;

/** @brief  Loader will be indicating weather the page has been loading or not*/
@property (strong, nonatomic)  UIActivityIndicatorView *activityIndicator;

/** @brief Payament delegates*/
@property(nullable, nonatomic, weak) id<PaymentGatewayDelegate> paymentDelegate;

/** @brief  Loading request using trakpay payamnet system*/
-(void)startPayment:(PaymentGatewayParams*)params;

@end





NS_ASSUME_NONNULL_END
