//
//  ViewController.h
//  ObjCSample
//
//  Created by Senthil Kumar Selvaraj on 03/05/21.
//

#import <UIKit/UIKit.h>

#import <PaymentGatewayObjC/PaymentGatewayObjC.h>

@interface ViewController : UIViewController<PaymentGatewayDelegate>

@property(nonatomic,strong) IBOutlet UITextView *responseTextView;

@end

