//
//  PaymentGatewayViewController.m
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/8/20.
//

#import "PaymentGatewayViewController.h"
#import <WebKit/WebKit.h>
#import <PaymentGatewayObjC/PaymentGatewayDelegate.h>
#import <PaymentGatewayObjC/Constants.h>

@interface NSString (StringFunctions)
+ (BOOL) hasCharacters:(NSString * )string ;
@end

@implementation NSString (StringFunctions)
+(BOOL)hasCharacters:(NSString * )string {
    if ((string == nil) || (string == (id)[NSNull null])) {
        return NO;
    }else {
        if([string length] == 0) {
            return NO;
        }
    }
    return YES;
}

+(NSString *)getValue:(NSString *)string {
    if ([NSString hasCharacters:string]){
        return  string;
    }else{
        return @"";
    }
}

@end

@interface PaymentGatewayViewController ()

@end

@implementation PaymentGatewayViewController

@synthesize paymentGatewayParams, paymentGatewayWebView, activityIndicator, paymentDelegate, url, transactionId, isUPIPaymentSelected, timer, upiUri;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self invalidateTimer];
    [self setNavigationTextAttributes];
    [self addLeftbarButton];
    // Do any additional setup after loading the view.
}

- (void)loadView{
    [super loadView];
    [self configureWebView];
    [self configureActivityIndicatorView];
}

// MARK:- Setup and configuration of webview and Loader

/**
  
@brief Configure the webview and Enabled JavaScript

 */

- (void)configureWebView{
    WKWebViewConfiguration *webConfiguration = [[WKWebViewConfiguration alloc] init];
    WKUserContentController *userController = [[WKUserContentController alloc] init];
    [userController addScriptMessageHandler:self name: kJavaScriptCallbackName];
    [userController addScriptMessageHandler:self name: kJavascriptUPIRequest];
    [userController addScriptMessageHandler:self name: kJavascriptUPICancel];
    [userController addScriptMessageHandler:self name: kJavascriptUPIContinue];
    WKPreferences *preference = [[WKPreferences alloc] init];
    [preference setJavaScriptCanOpenWindowsAutomatically:YES];
    webConfiguration.preferences = preference;
    webConfiguration.userContentController = userController;
    paymentGatewayWebView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:webConfiguration];
    [paymentGatewayWebView setUIDelegate:self];
    [paymentGatewayWebView setNavigationDelegate:self];
    [paymentGatewayWebView setCustomUserAgent: kUserAgent];
    [self.view addSubview:paymentGatewayWebView];
}

/**
  
 @brief Add the loading indicatior programattically

 */

- (void)configureActivityIndicatorView{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator setTintColor:[UIColor blackColor]];
    [activityIndicator setCenter:self.view.center];
    [self.view addSubview:activityIndicator];
}

/**
    
 @brief Showing the payment cancel comfirmation dialog

 */

-(void)cancelButtonClicked{
    
    UIAlertController *alertControler = [UIAlertController alertControllerWithTitle:nil message:kAlertMessage preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:kAlertButtonYes style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self invalidateTimer];
        [self.paymentDelegate didPaymentCanceled:self];
    }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:kAlertButtonNo style: UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertControler addAction:noAction];
    [alertControler addAction:yesAction];
    [self presentViewController:alertControler animated:YES completion:^{
            
    }];
}

// MARK:- Navigation Item configuration

/**
    
 @brief Define the naviagtion bar and bar button attributes

 */

- (void)setNavigationTextAttributes{
    NSDictionary *titleAttributes = @{NSForegroundColorAttributeName: [UIColor systemBlueColor]};
    [self.navigationController.navigationBar setTitleTextAttributes: titleAttributes];
    [self.navigationController.navigationBar setTintColor: [UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
    self.title = kNavigationTitle;
}

/**
    
 @brief Set cancel button in left naviagtion item

 */
- (void)addLeftbarButton{
    UIBarButtonItem *cancelButtonItem =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonClicked)];
    [cancelButtonItem setTintColor:[UIColor blackColor]];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
}

// MARK:- Payment methods

/**
 Making Payment request with PaymentGateway parameters
 
 @param params PaymentGatewayParams Model that has all the configuration values.
 
 @Note Make suere to  add all mandotory fields
 
 */

- (void)startPayment:(PaymentGatewayParams*)params{
    paymentGatewayParams = params;
    [self loadRquest:params];
}

/**
    @abstract Loading request using trakpay payamnet system
    
    @discussion Making POST request with PaymentGateway mandatory parameters
 
    @param params PaymentGatewayParams Model that has all the configuration values
 */
- (void)loadRquest:(PaymentGatewayParams *)params{
    NSString *htmlString = [self getHtlmForm:params];
    [[self paymentGatewayWebView] loadHTMLString:htmlString baseURL:nil];
}


/**
    @abstract Generate html form string using payament parameters
    
    @param params PaymentGatewayParams Model that has all the configuration values.
 
 */

- (NSString *)getHtlmForm:(PaymentGatewayParams *)params{
    
    return  [NSString stringWithFormat:@""
             "<form action=\"%@/v2/paymentrequest\" id=\"payment_form\" method=\"POST\">\n"
             "<input type=\"hidden\" value=\"%@\"   name=\"hash\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"api_key\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"return_url\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"mode\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"order_id\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"amount\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"currency\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"description\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"name\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"email\"/>"
             "<input type=\"hidden\" value=\"%@\"  name=\"phone\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"address_line_1\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"address_line_2\"/>"
             "<input type=\"hidden\" value=\"%@\"  name=\"city\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"state\"/>"
             "<input type=\"hidden\" value=\"%@\"  name=\"zip_code\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"country\"/>"
             "<input type=\"hidden\" value=\"%@\"  name=\"udf1\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"udf2\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"udf3\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"udf4\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"udf5\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"timeout_duration\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"return_url_failure\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"return_url_cancel\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"percent_tdr_by_user\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"flatfee_tdr_by_user\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"show_convenience_fee\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"split_enforce_strict\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"split_info\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"payment_options\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"payment_page_display_text\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"allowed_bank_codes\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"allowed_emi_tenure\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"allowed_bins\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"offer_code\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"product_details\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"enable_auto_refund\"/>"
             "<input type=\"hidden\" value=\"%@\"   name=\"interface_type\"/>"
             "<noscript><input type=\"submit\" value=\"Continue\"/></noscript>"
             "</form>"
             "<script>"
             "function formAutoSubmit () {"
                            "var payform = document.getElementById(\"payment_form\");"
                              "payform.submit();"
                           "}"
             "window.onload = formAutoSubmit;"
             "</script>",self.url, [NSString getValue: params.theHash], [NSString getValue: params.apiKey], [NSString getValue: params.returnURL], [NSString getValue: params.mode], [NSString getValue: params.orderID], [NSString getValue: params.amount], [NSString getValue: params.currency], [NSString getValue: params.theDescription], [NSString getValue: params.name],[NSString getValue: params.email] , [NSString getValue: params.phone], [NSString getValue: params.addressLine1], [NSString getValue: params.addressLine2], [NSString getValue: params.city], [NSString getValue: params.state], [NSString getValue: params.zipCode], [NSString getValue: params.country], [NSString getValue:params.udf1], [NSString getValue: params.udf2], [NSString getValue: params.udf3], [NSString getValue: params.udf4], [NSString getValue: params.udf5],[NSString getValue: params.timeout_duration], [NSString getValue: params.return_url_failure], [NSString getValue: params.return_url_cancel], [NSString getValue: params.percent_tdr_by_user], [NSString getValue: params.flatfee_tdr_by_user], [NSString getValue: params.show_convenience_fee], [NSString getValue: params.split_enforce_strict], [NSString getValue: params.split_info], [NSString getValue: params.payment_options], [NSString getValue: params.payment_page_display_text], [NSString getValue: params.allowed_bank_codes], [NSString getValue: params.allowed_emi_tenure], [NSString getValue: params.allowed_bins], [NSString getValue:params.offer_code], [NSString getValue: params.product_details], [NSString getValue: params.enable_auto_refund], [NSString getValue: params.interface_type]];
}

// MARK: -WKWebView Delegate methods

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    [activityIndicator startAnimating];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [activityIndicator stopAnimating];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
}

- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    
    if([message.name isEqual: kJavaScriptCallbackName]){
        if (isUPIPaymentSelected == YES){
            [self fetchUPIResponse:[self transactionId] apiKey:[paymentGatewayParams apiKey]];
        }else{
            [self invalidateTimer];
            [self.paymentDelegate didPaymentCompleted:self withData:message.body];
        }
    }
    else if([message.name isEqual: kJavascriptUPIRequest]){
        NSDictionary *body = (NSDictionary *) message.body;
        NSString *tranId = [body valueForKey:@"tran_id"];
        NSString *uri = [[body valueForKey:@"upi_uri"] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
        transactionId = tranId;
        [self openUPIUri:uri];
    }
    else if([message.name isEqual: kJavascriptUPICancel]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self cancelButtonClicked];
        });
    }
    else if([message.name isEqual: kJavascriptUPIContinue]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openUPIUri:self.upiUri];
        });
    }
}

-(void)openUPIUri:(NSString *)uri{
    NSURL *url = [NSURL URLWithString:uri];
    self.isUPIPaymentSelected = YES;
    self.upiUri = uri;
    [[UIApplication sharedApplication] openURL: url options:@{} completionHandler:^(BOOL success) {
        [self invalidateTimer];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(triggerUPIResponse) userInfo:NULL repeats:YES];
    }];
}

-(void)triggerUPIResponse{
    [self fetchUPIResponse:[self transactionId] apiKey:[paymentGatewayParams apiKey]];
}

-(void)invalidateTimer{
    [[self timer] invalidate];
    self.timer = nil;
}

-(void)fetchUPIResponse:(NSString*)transactionId apiKey:(NSString*)apiKey{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:kFetchUPIReponseAPIURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSData *data = [[NSString stringWithFormat:@"tran_id=%@&api_key=%@",transactionId, apiKey] dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [[session dataTaskWithRequest:request
        completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // convert to an object
        NSError * jsonError;
        NSDictionary * jsonObject = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data
                                                                                            options:NSJSONReadingAllowFragments
                                                                                              error: &jsonError];
        if(jsonObject != nil && [jsonObject valueForKey:@"payment_response"] != nil){
            NSDictionary *paymentResponse = [jsonObject valueForKey:@"payment_response"];
            NSNumber *statusCode = [paymentResponse valueForKey:@"response_code"];
            if (statusCode.integerValue != 1006){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *err;
                    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:paymentResponse options:0 error:&err];
                    NSString * responseString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                    [self invalidateTimer];
                    [self.paymentDelegate didPaymentCompleted:self withData:responseString];
                });
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.paymentDelegate didPaymentCompleted:self withData:nil];
            });
        }
        
    }] resume] ;

}

@end


