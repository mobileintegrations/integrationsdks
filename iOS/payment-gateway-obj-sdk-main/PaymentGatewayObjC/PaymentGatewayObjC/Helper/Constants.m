//
//  Constants.m
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 1/1/21.
//

#import "Constants.h"

@implementation Constants

NSString * const kNavigationTitle               = @"PaymentGateway";

NSString * const kJavaScriptCallbackName         = @"paymentResponseIos";

NSString * const kJavascriptUPIRequest          = @"paymentUpiRequestIos";

NSString * const kJavascriptUPICancel = @"cancelUpiPaymentIos";

NSString * const kJavascriptUPIContinue = @"continueUpiPaymentIos";

NSString * const kUserAgent                     = @"PaymentGateway-iOS-SDK";

NSString * const kStoryBoardName                = @"storyBoardName";

NSString * const kPaymentGatewayControllerIdentifier  = @"PaymentGatewayViewController";

NSString * const kAlertMessage                   = @"Do you want to cancel this payment?";

NSString * const kAlertButtonYes                = @"Yes";

NSString * const kAlertButtonNo                 = @"No";

NSString * const kFetchUPIReponseAPIURL = @"https://pgbiz.omniware.in/v2/fetchupiintentpaymentresponse";

@end
