//
//  Constants.h
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 1/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Constants : NSObject

/// Navigation Bar Title
extern NSString * const kNavigationTitle;

/// Payament Request URL
extern NSString * const kPaymentURL;

/// Javascript callback function name
extern NSString * const kJavaScriptCallbackName;

extern NSString * const  kJavascriptUPIRequest;

extern NSString * const  kJavascriptUPICancel;

extern NSString * const  kJavascriptUPIContinue;

/// Custom user agent for PaymentGateway
extern NSString * const kUserAgent;

/// StoryBoard Name
extern NSString * const kStoryBoardName;

/// Payment Contoller Identifier name
extern NSString * const kPaymentGatewayControllerIdentifier;

/// Confimation dialog message
extern NSString * const kAlertMessage;

/// Confimation Yes button title
extern NSString * const kAlertButtonYes;

/// Confimation No button title
extern NSString * const kAlertButtonNo;

extern NSString * const kFetchUPIReponseAPIURL;

@end

NS_ASSUME_NONNULL_END



