//
//  PaymentGatewayParams.m
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/10/20.
//


#import "PaymentGatewayParams.h"

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Private model interfaces

@interface PaymentGatewayParams (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

#pragma mark - JSON serialization

PaymentGatewayParams *_Nullable PaymentGatewayParamsFromData(NSData *data, NSError **error)
{
    @try {
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        return *error ? nil : [PaymentGatewayParams fromJSONDictionary:json];
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

PaymentGatewayParams *_Nullable PaymentGatewayParamsFromJSON(NSString *json, NSStringEncoding encoding, NSError **error)
{
    return PaymentGatewayParamsFromData([json dataUsingEncoding:encoding], error);
}

NSData *_Nullable PaymentGatewayParamsToData(PaymentGatewayParams *raknPayParams, NSError **error)
{
    @try {
        id json = [raknPayParams JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable PaymentGatewayParamsToJSON(PaymentGatewayParams *raknPayParams, NSStringEncoding encoding, NSError **error)
{
    NSData *data = PaymentGatewayParamsToData(raknPayParams, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

@implementation PaymentGatewayParams
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
        @"address_line_1": @"addressLine1",
        @"address_line_2": @"addressLine2",
        @"amount": @"amount",
        @"api_key": @"apiKey",
        @"city": @"city",
        @"country": @"country",
        @"currency": @"currency",
        @"description": @"theDescription",
        @"email": @"email",
        @"hash": @"theHash",
        @"mode": @"mode",
        @"name": @"name",
        @"order_id": @"orderID",
        @"phone": @"phone",
        @"return_url": @"returnURL",
        @"state": @"state",
        @"udf1": @"udf1",
        @"udf2": @"udf2",
        @"udf3": @"udf3",
        @"udf4": @"udf4",
        @"udf5": @"udf5",
        @"zip_code": @"zipCode",
        @"timeout_duration": @"timeout_duration",
        @"return_url_failure": @"return_url_failure",
        @"return_url_cancel": @"return_url_cancel",
        @"percent_tdr_by_user": @"percent_tdr_by_user",
        @"flatfee_tdr_by_user": @"flatfee_tdr_by_user",
        @"show_convenience_fee": @"show_convenience_fee",
        @"split_enforce_strict": @"split_enforce_strict",
        @"split_info": @"split_info",
        @"payment_options": @"payment_options",
        @"payment_page_display_text": @"payment_page_display_text",
        @"allowed_bank_codes": @"allowed_bank_codes",
        @"allowed_emi_tenure": @"allowed_emi_tenure",
        @"allowed_bins": @"allowed_bins",
        @"offer_code": @"offer_code",
        @"product_details": @"product_details",
        @"enable_auto_refund": @"enable_auto_refund",
        @"interface_type": @"interface_type"
    };
}

+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error
{
    return PaymentGatewayParamsFromData(data, error);
}

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return PaymentGatewayParamsFromJSON(json, encoding, error);
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PaymentGatewayParams alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PaymentGatewayParams.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (void)setNilValueForKey:(NSString *)key
{
    id resolved = PaymentGatewayParams.properties[key];
    if (resolved) [super setValue:@(0) forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PaymentGatewayParams.properties.allValues] mutableCopy];

    // Rewrite property names that differ in JSON
    for (id jsonName in PaymentGatewayParams.properties) {
        id propertyName = PaymentGatewayParams.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }

    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return PaymentGatewayParamsToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return PaymentGatewayParamsToJSON(self, encoding, error);
}
@end

NS_ASSUME_NONNULL_END

