//
//  PaymentGatewayDelegate.h
//  PaymentGatewaySwift
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/10/20.
//

#ifndef PaymentGatewayDelegate_h
#define PaymentGatewayDelegate_h

@protocol PaymentGatewayDelegate <NSObject>

/*!
    This method triggered once the payment completed
        
 @param controller PaymentGatewayacontroller
 @param data Payament success/failure json string  (String)
 */
-(void)didPaymentCompleted:(UIViewController *)controller withData:(nullable id)data;

/*!

 This method triggered once user click the cancel button
        
 @param controller PaymentGatewayacontroller
 */
-(void)didPaymentCanceled:(UIViewController *)controller;
@end

#endif /* PaymentGatewayDelegate_h */
