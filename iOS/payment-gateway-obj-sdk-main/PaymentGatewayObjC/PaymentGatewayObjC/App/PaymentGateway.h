//
//  PaymentGateway.h
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/8/20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <PaymentGatewayObjC/PaymentGatewayViewController.h>
#import <PaymentGatewayObjC/PaymentGatewayParams.h>
#import <PaymentGatewayObjC/PaymentGatewayDelegate.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentGateway : NSObject
/**
   @brief This is method present the payment screen
    @param from  from where it does present
    @param params  payment paremters
    @param delegate  PaymentGatewayadelegate - that is passed from partent controller
 */

+(void)present:(nonnull UIViewController *) from url:(NSString*)url delegate:(id<PaymentGatewayDelegate>)delegate withParams:(nonnull PaymentGatewayParams *)params;

+(void)open:(nonnull UIViewController *) from url:(NSString*)url delegate:(id<PaymentGatewayDelegate>)delegate withParams:(nonnull PaymentGatewayParams *)params;

@end



NS_ASSUME_NONNULL_END
