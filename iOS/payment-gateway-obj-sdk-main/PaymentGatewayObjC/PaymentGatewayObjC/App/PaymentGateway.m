//
//  PaymentGateway.m
//  PaymentGatewayObjC
//
//  Created by ​Senthil Kumar ​Selvaraj on 12/8/20.
//

#import "PaymentGateway.h"
#import "PaymentGatewayViewController.h"

@implementation PaymentGateway

/**
   @brief This is method present the payment screen
    @param from  from where it does present
    @param params  payment paremters
    @param delegate  PaymentGatewayadelegate - that is passed from partent controller
 */

+ (void)present:(UIViewController *)from
                              url:(NSString*)url
                         delegate:(id<PaymentGatewayDelegate>)delegate
                       withParams:(PaymentGatewayParams *)params{
   
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Checkout" bundle: [NSBundle bundleForClass: PaymentGatewayViewController.class]];
    
    PaymentGatewayViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"PaymentGatewayViewController"];
    
    [controller setPaymentDelegate: delegate];
    
    if([url hasSuffix:@"/"]){
        NSString *updatedURLString = [url substringToIndex:[url length]-1];
        [controller setUrl:updatedURLString];
    }else{
        [controller setUrl:url];
    }
    UINavigationController *navigationContoller = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [navigationContoller setModalPresentationStyle: UIModalPresentationFullScreen];
    
    [from presentViewController:navigationContoller animated:YES completion:^{
        /// triggering the payment here
        [controller startPayment: params];
    }];
    
}

+ (void)open:(UIViewController *)from
                              url:(NSString*)url
                         delegate:(id<PaymentGatewayDelegate>)delegate
                       withParams:(PaymentGatewayParams *)params{
   
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Checkout" bundle: [NSBundle bundleForClass: PaymentGatewayViewController.class]];
    
    PaymentGatewayViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"PaymentGatewayViewController"];
    
    [controller setPaymentDelegate: delegate];
    
    if([url hasSuffix:@"/"]){
        NSString *updatedURLString = [url substringToIndex:[url length]-1];
        [controller setUrl:updatedURLString];
    }else{
        [controller setUrl:url];
    }
    UINavigationController *navigationContoller = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [navigationContoller setModalPresentationStyle: UIModalPresentationFullScreen];
    
    [from presentViewController:navigationContoller animated:YES completion:^{
        /// triggering the payment here
        [controller startPayment: params];
    }];
    
}

@end
