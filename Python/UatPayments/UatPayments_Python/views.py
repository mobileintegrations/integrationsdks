from django.shortcuts import render, render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
import datetime
import hashlib
from random import randint
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.template.context_processors import csrf

def Payment_Request(request):
	API_KEY = "9b621589e-52b-7821-b836-b630edad99dg"
	SALT = "1845896245dd586se913fa536be8dbf237a6c15ee"
	URL = "https://pgbiz.omniware.in/v2/paymentrequest"
	posted={}
	for i in request.POST:
		posted[i]=request.POST[i]
	
	hashSequence = "address_line_1|address_line_2|amount|api_key|city|country|currency|description|email|mode|name|order_id|phone|return_url|state|udf1|udf2|udf3|udf4|udf5|zip_code";
	
	hash_string=SALT
	hashVarsSeq=hashSequence.split('|')
	
	for i in hashVarsSeq:
		if i in posted.keys():
			if len(str(posted[i])) > 0: 
				hash_string+='|'
				hash_string+=str(posted[i])
	
	hash=hashlib.sha512(hash_string.encode()).hexdigest().upper()
	action =URL
		
	if(	posted.get("amount")!='' and posted.get("amount")!=None and
		posted.get("address_line_1")!='' and posted.get("address_line_1")!=None and	
		posted.get("city")!='' and posted.get("city")!=None and	
		posted.get("name")!='' and posted.get("name")!=None and	
		posted.get("email")!='' and posted.get("email")!=None and	
		posted.get("phone")!='' and posted.get("phone")!=None and	
		posted.get("order_id")!='' and posted.get("order_id")!=None and	
		posted.get("currency")!='' and posted.get("currency")!=None and	
		posted.get("description")!='' and posted.get("description")!=None and	
		posted.get("country")!='' and posted.get("country")!=None and	
		posted.get("return_url")!='' and posted.get("return_url")!=None ):
		
		return render_to_response('payment_request.html',{"posted":posted,"hash":hash,"API_KEY":API_KEY,"action":action },RequestContext(request))
	else:
		hash=''
		return render_to_response('payment_request.html',{"posted":posted,"hash":hash,"API_KEY":API_KEY,"action":"." },RequestContext(request))

@csrf_protect
@csrf_exempt
def Payment_Response(request):
	c = {}
	c.update(csrf(request))
	response_code=request.POST["response_code"]
	transaction_id=request.POST["transaction_id"]
	response_message=request.POST["response_message"]
	amount=request.POST["amount"]
	hash=request.POST["hash"]
	SALT = "18e6063d410586se913fa536be8dbf237a6c15ee"
	hash_string=SALT
	
	for i in sorted(request.POST):
		if(i!='hash'):
			if len(request.POST[i]) > 0: 
				hash_string+='|'
				hash_string+=request.POST[i]
				
	calculated_hash=hashlib.sha512(hash_string.encode()).hexdigest().upper()
	if(hash == calculated_hash):			
		if(response_code == '0'):
			return render_to_response('sucess.html',{"txnid":transaction_id,"status":response_message,"amount":amount})
		else:	
			return render_to_response("failure.html",{"response_message":"Transaction Failed"},RequestContext(request))
	else:
			return render_to_response("failure.html",{"response_message":"Hash Mis matched , Transaction Failed"},RequestContext(request))
		
