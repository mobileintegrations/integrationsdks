from django.apps import AppConfig


class UatPaymentsPythonConfig(AppConfig):
    name = 'UatPayments_Python'
